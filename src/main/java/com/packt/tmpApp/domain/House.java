package com.packt.tmpApp.domain;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dawid
 */
@Entity
@Table(name="House")
public class House {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private int idHouse;

    @JsonView(Views.Public.class)
    private String address;

    @OneToMany(mappedBy = "house")
    @JsonView(Views.House.class)
    private Set<UserInHouse> userInHouse = new HashSet<UserInHouse>();

    public void addUserInHouse(UserInHouse userInHouse) {
        this.userInHouse.add(userInHouse);
    }
    
    public House() {

    }

    public House(int idHouse) {
        this.idHouse = idHouse;
        this.address = address;
    }

    public House(String address) {
        this.address = address;
    }

    public House(int idHouse, String address) {
        this.idHouse = idHouse;
        this.address = address;
    }

    public House(String address, Set<UserInHouse> userInHouse) {
        this.address = address;
        this.userInHouse = userInHouse;
    }

    public House(int idHouse, String address, Set<UserInHouse> userInHouse) {
        this.idHouse = idHouse;
        this.address = address;
        this.userInHouse = userInHouse;
    }

    public int getIdHouse() {
        return idHouse;
    }

    public void setIdHouse(int idHouse) {
        this.idHouse = idHouse;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<UserInHouse> getUserInHouse() {
        return userInHouse;
    }

    public void setUserInHouse(Set<UserInHouse> userInHouse) {
        this.userInHouse = userInHouse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        House house = (House) o;

        return idHouse == house.idHouse;
    }

    @Override
    public int hashCode() {
        return idHouse;
    }
}