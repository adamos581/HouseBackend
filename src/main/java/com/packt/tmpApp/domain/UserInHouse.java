package com.packt.tmpApp.domain;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;

/**
 *
 * @author dawid
 */
@Entity
@Table(name="UserInHouse")
@IdClass(UserInHousePK.class)
public class UserInHouse {

    @Id
    @ManyToOne()
    @JoinColumn(name = "accountLogin")
    @JsonView(Views.House.class) // <- brak widoczności usera
    private UserAccount user;

    @Id
    @ManyToOne()
    @JoinColumn(name = "idHouse")
    @JsonView(Views.User.class)
    private House house;

    //additional fields
    @JsonView(Views.Public.class)
    private String houseNick;
    @JsonView(Views.Public.class)
    private String userRole;
    
    public UserInHouse() {
    }

    public UserInHouse(UserAccount user, House house, String houseNick, String userRole) {
        this.user = user;
        this.house = house;
        this.houseNick = houseNick;
        this.userRole = userRole;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public String getHouseNick() {
        return houseNick;
    }

    public void setHouseNick(String houseNick) {
        this.houseNick = houseNick;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInHouse that = (UserInHouse) o;

        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return house != null ? house.equals(that.house) : that.house == null;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (house != null ? house.hashCode() : 0);
        return result;
    }
}