package com.packt.tmpApp.domain;

import java.io.Serializable;

/**
 *
 * @author dawid
 */
public class UserInHousePK implements Serializable {

    private UserAccount user;

    private House house;

    public UserInHousePK() {
    }

    public UserInHousePK(UserAccount user, House house) {
        this.user = user;
        this.house = house;
    }

    public UserAccount getUser() {
        return user;
    }

    public void setUser(UserAccount user) {
        this.user = user;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserInHousePK that = (UserInHousePK) o;

        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        return house != null ? house.equals(that.house) : that.house == null;
    }

    @Override
    public int hashCode() {
        int result = user != null ? user.hashCode() : 0;
        result = 31 * result + (house != null ? house.hashCode() : 0);
        return result;
    }
}
