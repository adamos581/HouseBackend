package com.packt.tmpApp.domain;

import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author dawid
 */
@Entity
@Table(name="UserAccount")
public class UserAccount {
    
    @Id
    @JsonView(Views.Public.class)
    private String login;

    @JsonView(Views.Public.class)
    private String email;

    @JsonView(Views.Public.class)
    private String passwordHash;

    @OneToMany(mappedBy = "user")
    @JsonView(Views.User.class)
    private Set<UserInHouse> userInHouse = new HashSet<UserInHouse>();

    public void addUserInHouse(UserInHouse userInHouse) {
        this.userInHouse.add(userInHouse);
    }

    public UserAccount() {
    }

    public UserAccount(String login, String email, String passwordHash, Set<UserInHouse> userInHouse) {
        this.login = login;
        this.email = email;
        this.passwordHash = passwordHash;
        this.userInHouse = userInHouse;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    
    //TO DO: Hash the password
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Set<UserInHouse> getUserInHouse() {
        return userInHouse;
    }

    public void setUserInHouse(Set<UserInHouse> userInHouse) {
        this.userInHouse = userInHouse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserAccount that = (UserAccount) o;

        return login != null ? login.equals(that.login) : that.login == null;
    }

    @Override
    public int hashCode() {
        return login != null ? login.hashCode() : 0;
    }
}
