package com.packt.tmpApp.domain;

import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name="LogonEvent")
public class LogonEvent {
    
    @Id
    private Integer idEvent;
    private String accountLogin;
    private Date dateEvent;
    private String device;
    private String ipAddress;
    
    public LogonEvent() {
        super();
    }
    
    public LogonEvent(Integer idEvent, String accountLogin, Date dateEvent,
            String device, String ipAddress) {
        this.idEvent = idEvent;
        this.accountLogin = accountLogin;
        this.dateEvent = dateEvent;
        this.device = device;
        this.ipAddress = ipAddress;
    }
    
    public Integer getIdEvent() {
        return this.idEvent;
    }
    
    public String getAccountLogin() {
        return this.accountLogin;
    }
    
    public Date getDateEvent() {
        return this.dateEvent;
    }
    
    public String getDevice() {
        return this.device;
    }
    
    public String getIpAddress() {
        return this.ipAddress;
    }
    
    public void setIdEvent(Integer idEvent) {
        this.idEvent = idEvent;
    }
    
    public void setAccountLogin(String accountLogin) {
        this.accountLogin = accountLogin;
    }
    
    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }
    
    public void setDevice(String device) {
        this.device = device;
    }
    
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(this.getClass() != obj.getClass())
            return false;
        
        LogonEvent other = (LogonEvent) obj;
        if(this.idEvent == null) {
            if(other.idEvent != null)
                return false;
        }
        else if(!this.idEvent.equals(other.idEvent))
            return false;
        return true;
    }
    
    @Override
    public int hashCode() {
        return 17 + ((this.idEvent == null) ? 0 : this.idEvent.hashCode());
    }
    
}