/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.exception;

/**
 *
 * @author staro
 */
public class CreateUserException extends Exception
{
    public CreateUserException(String message)
    {
        super(message);
    }
}
