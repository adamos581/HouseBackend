/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.service;

import com.packt.tmpApp.domain.House;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author staro
 */
public interface HouseService {
    @PreAuthorize("hasRole('USER')")
    Integer create(House entity);

    @PostAuthorize("isHouseOwner(principal.username) " +
            "or isHouseTenant(principal.username) " +
            "or hasRole('ADMIN')")
    House read(Integer key);

    @PreAuthorize("hasRole('ADMIN')")
    Iterable<House> readAll();
}
