/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.service.impl;

import com.packt.tmpApp.domain.House;
import com.packt.tmpApp.repository.HouseRepository;
import com.packt.tmpApp.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author staro
 */
@Service
@Transactional
public class HouseServiceImpl implements HouseService{
   
    
    @Autowired
    HouseRepository houseRepository;

    @Override
    public Integer create(House entity) {
        return houseRepository.create(entity);
    }

    @Override
    public House read(Integer key) {
        return houseRepository.read(key);
    }

    @Override
    public Iterable<House> readAll() {
        return houseRepository.readAll();
    }
    
}
