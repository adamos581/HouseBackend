/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.service.impl;

import com.packt.tmpApp.domain.UserAccount;
import com.packt.tmpApp.exception.CreateUserException;
import com.packt.tmpApp.repository.UserAccountRepository;
import com.packt.tmpApp.service.UserService;
import java.util.HashSet;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author zaki
 */
@Service
@Transactional
public class UserServiceImpl implements UserService{
   
    private static final String PASSWORD_PATTERN =
              "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$.%]).{8,32})";
    private static final String LOGIN_PATTERN =
              ".{6,20}";
    
    @Autowired
    UserAccountRepository userAccountRepository;
    //TO DO:
    //return null when password is bad or login's too long/short, 
    //maybe create exception for error indication
    @Override
    public String create(UserAccount entity) throws CreateUserException{
        String password = entity.getPasswordHash();
        boolean passwordMatch = password.matches(PASSWORD_PATTERN);
        boolean loginMatch = entity.getLogin().matches(LOGIN_PATTERN);
        
        /*password should contain: ;
        -at least 8 characters but no more than 32
        -one special sign ('@', '#','$','%','.', etc.)
        -One capital letter
        -one number
            login should be between 6-20 letters long
        */
        if(passwordMatch && loginMatch)
        {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(13); // encoding strenght between 4 and 31
            String hash = encoder.encode(password);
            entity.setPasswordHash(hash);
        return userAccountRepository.create(entity);
        }
        else if(!passwordMatch)
        {
            throw new CreateUserException("Your password is not secure enought!");
        }
    else if(!loginMatch)
    {
        throw new CreateUserException("Your login is too short or too long!");
    }
    else throw new CreateUserException("Something went wrong when creating new user. :(");
    }
    
    @Override
    public UserAccount read(String key) {
        return userAccountRepository.read(key);
    }
    
    @Override
    public Iterable<UserAccount> readAll() {
        return userAccountRepository.readAll();
    }
}
