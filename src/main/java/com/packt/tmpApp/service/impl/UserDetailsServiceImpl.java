package com.packt.tmpApp.service.impl;

import com.packt.tmpApp.domain.UserAccount;
import com.packt.tmpApp.repository.UserAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
            UserAccountRepository userAccountRepository;
    
    @Override
    public UserDetails loadUserByUsername(String string) throws UsernameNotFoundException {
        UserAccount userAccount = userAccountRepository.read(string);
        if(userAccount == null){
            throw new UsernameNotFoundException("No such user : "+string);
        }
        
        String[] roles = new String[1];
        roles[0] = "ROLE_USER";
        if(string.equals("admin")) roles[0] = "ROLE_ADMIN";
        return new User(userAccount.getLogin(), userAccount.getPasswordHash(),
                AuthorityUtils.createAuthorityList(roles));
    }
}
