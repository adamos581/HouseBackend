/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.service;

import com.packt.tmpApp.domain.UserAccount;
import com.packt.tmpApp.exception.CreateUserException;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author staro
 */
public interface UserService {
    String create(UserAccount entity) throws CreateUserException;

    @PreAuthorize("principal.username == #key or hasRole('ADMIN')")
    UserAccount read(String key);

    @PreAuthorize("hasRole('ADMIN')")
    Iterable<UserAccount> readAll();
}
