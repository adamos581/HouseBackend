package com.packt.tmpApp.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.packt.tmpApp.domain.UserAccount;
import com.packt.tmpApp.domain.Views;
import com.packt.tmpApp.exception.CreateUserException;
import com.packt.tmpApp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @JsonView(Views.User.class)
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity<Iterable<UserAccount>> getUserAccount(){
        return new ResponseEntity<Iterable<UserAccount>>(
                userService.readAll(), HttpStatus.OK);
    }

    @JsonView(Views.User.class)
    @RequestMapping(value = "/{login}", method = RequestMethod.GET)
    ResponseEntity<UserAccount> getUserAccount(@PathVariable String login) {
        UserAccount user = this.userService.read(login);
        return new ResponseEntity<UserAccount>(user, HttpStatus.OK);
    }

    @JsonView(Views.User.class)
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<UserAccount> getUserAccount(@RequestBody UserAccount newUser){
        if(newUser != null){
            newUser.getUserInHouse().forEach(userInHouse -> userInHouse.setUser(newUser));
            try {
                userService.create(newUser);
            } catch (CreateUserException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<UserAccount>(newUser, HttpStatus.OK);
    }
}
