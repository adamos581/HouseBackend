package com.packt.tmpApp.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.packt.tmpApp.domain.House;
import com.packt.tmpApp.domain.Views;
import com.packt.tmpApp.service.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/house")
public class HouseController {

    @Autowired
    HouseService houseService;

    @JsonView(Views.House.class)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    House getUserAccount(@PathVariable Integer id) {
        House house = this.houseService.read(id);
        return house;
    }

    @JsonView(Views.House.class)
    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity createHouse(@RequestBody House newHouse){
        if(newHouse != null){
            newHouse.getUserInHouse().forEach(userInHouse -> userInHouse.setHouse(newHouse));
            this.houseService.create(newHouse);
        }
        return new ResponseEntity<>(newHouse, HttpStatus.OK);
    }

    @JsonView(Views.House.class)
    @RequestMapping(method = RequestMethod.GET)
    ResponseEntity getAllHouses(){
        Iterable<House> houses = this.houseService.readAll();
        return new ResponseEntity<>(houses, HttpStatus.OK);
    }
}
