package com.packt.tmpApp.repository.impl;

import com.packt.tmpApp.domain.House;
import com.packt.tmpApp.repository.HouseRepository;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class HouseRepositoryImpl implements HouseRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public Integer create(House entity) {
        Session session = sessionFactory.getCurrentSession();

        entity.getUserInHouse().forEach(userInHouse ->
                session.saveOrUpdate(userInHouse.getUser()));

        session.saveOrUpdate(entity);

        entity.getUserInHouse().forEach(session::saveOrUpdate);

        return null;
    }

    @Override
    @Transactional
    public House read(Integer key) {
        Session session = this.sessionFactory.getCurrentSession();
        House house = session.get(House.class, key);
        Hibernate.initialize(house.getUserInHouse());
        return house;
    }

    @Override
    public Iterable<House> readAll() {
        return null;
    }

    @Override
    public void update(House entity) {

    }

    @Override
    public void delete(House entity) {

    }
}
