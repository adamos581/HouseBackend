/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.repository.impl;

import com.packt.tmpApp.domain.House;
import com.packt.tmpApp.domain.UserAccount;
import com.packt.tmpApp.domain.UserInHouse;
import com.packt.tmpApp.domain.Views;
import com.packt.tmpApp.repository.UserAccountRepository;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.soap.SOAPBinding;

/**
 *
 * @author adambelniak
 */
@Repository
public class UserAccountRepositoryImpl implements UserAccountRepository {

    @Autowired
    public SessionFactory sessionFactory;

    @Override
    @Transactional
    public String create(UserAccount entity) {
        Session session = sessionFactory.getCurrentSession();

        entity.getUserInHouse().forEach(userInHouse ->
            session.saveOrUpdate(userInHouse.getHouse()));

        session.saveOrUpdate(entity);

        entity.getUserInHouse().forEach(session::saveOrUpdate);

        return null;
    }

    @Override
    @Transactional
    public UserAccount read(String key) {
        Session session = this.sessionFactory.getCurrentSession();
        UserAccount user =  session.get(UserAccount.class, key);
        Hibernate.initialize(user.getUserInHouse());
        return user;
    }

    @Override
    @Transactional
    public Iterable<UserAccount> readAll() {
        Session session = sessionFactory.getCurrentSession();
        Iterable<UserAccount> users = session.createQuery("FROM " + UserAccount.class.getSimpleName()).list();
        users.forEach(user -> Hibernate.initialize(user.getUserInHouse()));
        return users;
    }

    @Override
    public void update(UserAccount entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(UserAccount entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
