/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.repository.impl;

import com.packt.tmpApp.domain.UserInHouse;
import com.packt.tmpApp.repository.UserInHouseRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserInHouseRepositoryImpl implements UserInHouseRepository {
    
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Integer create(UserInHouse entity) {
        Session session = sessionFactory.getCurrentSession();
        return (Integer) session.save(entity);
    }

    @Override
    public UserInHouse read(Integer key) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(UserInHouse.class, key);
    }

    @Override
    public Iterable<UserInHouse> readAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createQuery("FROM " + UserInHouse.class.getSimpleName()).list();
    }

    @Override
    public void update(UserInHouse entity) {
        Session session = sessionFactory.getCurrentSession();
        session.update(entity);
    }

    @Override
    public void delete(UserInHouse entity) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(entity);
    }
    
}
