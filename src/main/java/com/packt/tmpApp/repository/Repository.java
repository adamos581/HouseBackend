/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.repository;

/**
 *
 * @author zaki
 */
public interface Repository<Type, Key> {
    public Key create(Type entity);
    public Type read(Key key);
    public Iterable<Type> readAll();
    public void update(Type entity);
    public void delete(Type entity);
}
