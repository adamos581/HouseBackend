/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.packt.tmpApp.repository;

import com.packt.tmpApp.domain.UserInHouse;

/**
 *
 * @author zaki
 */
public interface UserInHouseRepository 
        extends Repository<UserInHouse, Integer>{
    
}
