package com.packt.tmpApp.repository;

import com.packt.tmpApp.domain.House;

public interface HouseRepository extends Repository<House, Integer> {
}
