package com.packt.tmpApp.config.security;

import com.packt.tmpApp.domain.House;
import com.packt.tmpApp.domain.UserInHouse;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

import java.util.Optional;

public class CustomMethodSecurityExpressionRoot
        extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private Object filterObject;
    private Object returnObject;
    public CustomMethodSecurityExpressionRoot(Authentication authentication) {
        super(authentication);
    }

    public boolean isHouseOwner(String userName) {
        House house = (House) getReturnObject();
        Optional<UserInHouse> uih = house.getUserInHouse().stream()
                .filter(o -> o.getUser().getLogin().equals(userName))
                .findFirst();
        return uih.map(userInHouse -> userInHouse.getUserRole().equals("OWNER")).orElse(false);
    }

    public boolean isHouseTenant(String userName) {
        House house = (House) getReturnObject();
        Optional<UserInHouse> uih = house.getUserInHouse().stream()
                .filter(o -> o.getUser().getLogin().equals(userName))
                .findFirst();
        return uih.map(userInHouse -> userInHouse.getUserRole().equals("TENANT")).orElse(false);
    }

    @Override
    public void setFilterObject(Object o) {
        this.filterObject = o;
    }

    @Override
    public Object getFilterObject() {
        return this.filterObject;
    }

    @Override
    public void setReturnObject(Object o) {
        this.returnObject = o;
    }

    @Override
    public Object getReturnObject() {
        return this.returnObject;
    }

    @Override
    public Object getThis() {
        return this;
    }
}
